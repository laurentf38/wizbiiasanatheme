function hide (cn) {
  items = document.getElementsByClassName(cn)
  for (let i = 0; i < items.length; i++) {
  items[i].style.display = 'none'
  }
}

function run () {
  items = document.getElementsByClassName('BoardCardWithCustomProperties')
  for (let i = 0; i < items.length; i++) {
    items[i].style.borderTop = '3px solid #3DF9C6'
  }
  
  items = document.getElementsByClassName('BoardCardWithCustomProperties-contents')
  for (let i = 0; i < items.length; i++) {
    items[i].style.padding = '10px'
  }
  
  items = document.getElementsByClassName('BoardCardWithCustomProperties-assignee')
  for (let i = 0; i < items.length; i++) {
    items[i].style.position = 'absolute'
    items[i].style.bottom = '5px'
    items[i].style.right = '-5px'
  }
  
  items = document.getElementsByClassName('BoardCardWithCustomProperties-assigneeAndDueDate')
  for (let i = 0; i < items.length; i++) {
    items[i].style.height = '0px'
  }
  
  items = document.getElementsByClassName('BoardCardWithCustomProperties-name')
  for (let i = 0; i < items.length; i++) {
    txt = items[i].textContent
  
    parts = txt.split('-')
    if (parts.length == 2) {
        newTxt = '<span style="font-weight: bold">' + parts[0] + '</span> — ' + parts[1]
        items[i].innerHTML = newTxt
    }
  
    if (txt.substr(0,2) == '🐝') {
        items[i].parentElement.parentElement.parentElement.style.borderTop = '3px solid #F9ED20'
    }
    else if (txt.substr(0,2) == '💰') {
        items[i].parentElement.parentElement.parentElement.style.borderTop = '3px solid #43CEFF'
    }
    else if (txt.substr(0,2) == '🐛') {
        items[i].parentElement.parentElement.parentElement.style.borderTop = '3px solid #FF5C60'
    }
  }
  
  hide('BoardCardWithCustomProperties-customProperties')
  hide('BoardCardWithCustomProperties-rightMetadata')
  hide('DueDateContainer-dueDate')
  hide('DueDateContainer-button')
  
  // Lists
  
  lists = document.getElementsByClassName('BoardColumn BoardBody-column BoardBody-columnInBoardWithViewMenu')
  lists[2].style.opacity = '0.7' // Running
}

setInterval(() => { run() }, 500)