# Custom theme for Asana

## How to install?

1. Download this repo as .zip
2. Unzip it
3. Go to chrome://extensions/
4. Click on **Load unpacked** on the top left corner
5. Select the `crx/` folder
6. You good to go